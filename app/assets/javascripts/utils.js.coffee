# Formats amount strings
@amountFormat = (amount) ->
  '$' + Number(amount).toLocaleString()
