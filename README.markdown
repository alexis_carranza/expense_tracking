Introduction to React.js

Mock expense tracking app using react-rails.

Requirements:
        1. When the user creates a new record through the horizontal form, it will be appended to the records table.
        2. The user will be able to inline-edit any existing record.
        3. Clicking on any Delete button will remove the associated record from the table.
        4. Adding, editing or removing an existing record will update the amount boxes at the top of the page.
